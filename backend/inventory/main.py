from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from redis_om import get_redis_connection, HashModel

app = FastAPI()

# Middleware to whitelist from Browser side API call from specific frontend port different than uvicorn
app.add_middleware(
    CORSMiddleware,
    allow_origins=['http://localhost:3000'],
    allow_methods=['*'],
    allow_headers=['*']
)    

# redis connection
redis = get_redis_connection(
     host="127.0.0.1",
     port=6379,
     password="the_super_password",
     decode_responses=True
)

# Create our data model
class Product(HashModel):
    name: str
    price: float
    quantity: int

    # Map this model to redis connection
    class Meta:
        database = redis
          

def format(pk: str):
    product = Product.get(pk)
    return {
        'id': product.pk,
        'name': product.name,
        'price': product.price,
        'quantity': product.quantity
    }

@app.get("/products_format")
async def all():
    return [
        format(pk) for pk in Product.all_pks()
    ]

   
@app.get("/products")
async def all():
    return [
        Product.get(pk) for pk in Product.all_pks()
    ]

@app.post("/products")
async def create(product: Product):
    return product.save()

@app.get("/products/{pk}")
async def get(pk: str):
    return Product.get(pk)

@app.delete("/products/{pk}")
async def delete(pk: str):
    return Product.delete(pk)
