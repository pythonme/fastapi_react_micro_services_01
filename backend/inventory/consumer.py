from main import redis, Product
import time

# To consume this event, we need the name of event and the comsumer group
key = 'order_completed'
group = 'inventory-group'

# Try to create the comsumer group
try:
    redis.xgroup_create(key, group)
except:
    print('Group already exists!')


# Let's consume
while True:
    try:
        # This is how we consume an event 
        results = redis.xreadgroup(group, key, {key: '>'}, None)
        #print(results)
        
        # Decrease product quantity
        if results != []:
            for result in results:
                obj = result[1][0][1]

                try:
                    product = Product.get(obj['product_id'])
                    # Check if the product has not been deleted during the order
                    print(product)
                    product.quantity = product.quantity - int(obj['quantity'])
                    product.save()
                except:
                    # Fire an event to get refund
                    redis.xadd('refund_order', order.dict(),'*')


    except Exception as e:
        print(str(e))
        
    time.sleep(1)
