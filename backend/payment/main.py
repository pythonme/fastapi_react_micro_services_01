from fastapi import FastAPI, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.background import BackgroundTasks
from redis_om import get_redis_connection, HashModel
import requests, time

app = FastAPI()

# Middleware to whitelist from Browser side API call from specific frontend port different than uvicorn
app.add_middleware(
    CORSMiddleware,
    allow_origins=['http://localhost:3000'],
    allow_methods=['*'],
    allow_headers=['*']
)    
 
# redis connection
redis = get_redis_connection(
     host="127.0.0.1",
     port=6379,
     password="the_super_password",
     decode_responses=True
)

# Create our data model
class Order(HashModel):
    product_id: str
    price: float
    fee: float
    total: float
    quantity: int
    status: str # pending, completed, refunded

    # Map this model to redis connection
    class Meta:
        database = redis
          

@app.get("/orders/{pk}")
async def get(pk: str):
    order = Order.get(pk)
    #redis.xadd('refund_order', order.dict(),'*')
    return order

@app.post("/orders")
async def create(request: Request, background_tasks: BackgroundTasks): # Product ID and Quantity
    # Get product id from body
    body = await request.json()

    # Call the inventory API to get Product name and price (it's a way to communicate with another microservice)
    req = requests.get(f"http://localhost:8000/products/{body['id']}" )
    product = req.json()

    # Create an order from product info
    order = Order(
        product_id = body['id'],
        price = product['price'],
        fee = 0.2 * product['price'], # 20% fee
        total = 1.2 * product['price'],
        quantity = body['quantity'],
        status = 'pending'
    )
    order.save()

    # FastAPI will run in parallel the function 'order_completed' as a background task
    background_tasks.add_task(order_completed, order)

    return order

# With this function, we fake that the order is processed and completed in 5 secs
def order_completed(order: Order):
    time.sleep(5)
    order.status = 'completed'
    order.save()
    # Once the order is completed, we fire an event via redis and let the inventory microservice detect it (it's a way to communicate with other microservice via redis)
    # so we create an object named 'order_completed', this object is a dictonary type, * is a auto gen ID 
    redis.xadd('order_completed', order.dict(),'*')




@app.delete("/orders/{pk}")
async def delete(pk: str):
    return Order.delete(pk)
