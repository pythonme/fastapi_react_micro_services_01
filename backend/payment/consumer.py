from main import redis, Order
import time

# To consume this event, we need the name of event and the comsumer group
key = 'refund_order'
group = 'payment-group'

# Try to create the comsumer group
try:
    redis.xgroup_create(key, group)
except:
    print('Group already exists!')


# Let's consume
while True:
    try:
        # This is how we consume an event 
        results = redis.xreadgroup(group, key, {key: '>'}, None)
        #print(results)
        
        # Decrease product quantity
        if results != []:
            for result in results:
                obj = result[1][0][1]
                order = Order.get(obj['pk'])
                print(order)
                order.status = 'refunded'
                order.save()

    except Exception as e:
        print(str(e))
        
    time.sleep(1)
